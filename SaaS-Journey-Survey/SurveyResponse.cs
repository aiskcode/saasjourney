﻿using System.Collections.Generic;

namespace SaaSJourneySurvey
{
    public class SurveyResponse
    {
        public string q1 { get; set; }
        public string q2 { get; set; }
        public List<string> q3 { get; set; }
        public string q4 { get; set; }
        public string q5 { get; set; }
        public string q6 { get; set; }
        public string q7 { get; set; }
        public string q8 { get; set; }
        public string q9 { get; set; }
        public string q10 { get; set; }
        public string q11 { get; set; }
        public string q12 { get; set; }
        public string q13 { get; set; }
        public string q14 { get; set; }
        public string q15 { get; set; }
        public string q16 { get; set; }
        public string q17 { get; set; }
        public string q18 { get; set; }
        public string q19 { get; set; }
        public string q28 { get; set; }
        public string q27 { get; set; }
        public string q26 { get; set; }
        public string q25 { get; set; }
        public string q24 { get; set; }
        public string q23 { get; set; }
        public string q22 { get; set; }
        public string q20 { get; set; }
        public string email { get; set; }
        public string product_category { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
    }
}
