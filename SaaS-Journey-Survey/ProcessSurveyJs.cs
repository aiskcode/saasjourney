using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

namespace SaaSJourneySurvey
{
    public static class ProcessSurveyJs
    {
        [FunctionName("ProcessSurveyJs")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            dynamic data = await req.Content.ReadAsStringAsync();
            // Get request body
            var e = JsonConvert.DeserializeObject<SurveyResponse>(data as string);
            var businessScoreQuestions = new List<string> { "q2", "q3", "q4", "q5", "q6", "q7", "q20", "q23" };
            var techScoreQuestions = new List<string> { "q7", "q8", "q9", "q10", "q11", "q12", "q13", "q14", "q15", "q16", "q17", "q18", "q19", "q20", "q21", "q24", "q25", "q26" };
            var pricingQuestions = new List<string> { "q3" };
            var supportQuestions = new List<string> { "q4", "q5", "q6" };
            var salesQuestions = new List<string> { "q7", "q13", "q14", "q15" };
            var scalabilityQuestions = new List<string> { "q8", "q9", "q10", "q11", "q12" };
            var appScalabilityQuestions = new List<string> { "q18", "q19", "q20", "q21", "q22", "q23", "q24", "q25" };
            var extensabilityQuestions = new List<string> { "q26", "q27", "q28" };
            var overView = new OverallResponseView
            {
                ResponseId = Guid.NewGuid().ToString(),
                RespondantName = e.first_name,
                ResponandtLastName = e.last_name,
                ProductCategory = e.product_category,
                RespondantEmail = e.email,
                Company = e.company,
                SupportScore = 0,
                PricingScore = 0,
                ExtensibilityScore = 0,
                AppScalabilityScore = 0,
                ScalabilityScore = 0,
                SalesScore = 0,
                MappedResponses = new List<MappedResponse>()
            };
            try
            {
                var classProperties = e.GetType().GetProperties();
                foreach (var item in classProperties)
                {

                    var value = e.GetType().GetProperty(item.Name).GetValue(e);
                    if (item.Name == "q1")
                    {
                        overView.ProductName = value.ToString();
                    }

                    else
                    {

                        var response = new MappedResponse();
                        response.QuestionId = item.Name;
                        if (value != null)
                        {
                            if (item.Name == "q3")
                            {
                                var array = value as List<string>;
                                var tempScore = 0;
                                for (int i = 0; i < array.Count; i++)
                                {
                                    tempScore += int.Parse(array[i]);
                                }
                                response.Score = tempScore;
                            }
                            else
                            {
                                response.Score = int.Parse(value.ToString());
                            }
                        }
                        else
                        {
                            response.Score = 0;
                        }
                        overView.MappedResponses.Add(response);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var results = GetAllSuggestions();
            foreach (var item in overView.MappedResponses)
            {
                overView.OverallScore = overView.OverallScore + item.Score;
                if (businessScoreQuestions.Contains(item.QuestionId)) overView.BusinessScore = overView.BusinessScore + item.Score;
                else if (techScoreQuestions.Contains(item.QuestionId)) overView.TechnicalScore = overView.TechnicalScore + item.Score;
                if (pricingQuestions.Contains(item.QuestionId)) overView.PricingScore = overView.PricingScore + item.Score;
                else if (supportQuestions.Contains(item.QuestionId)) overView.SupportScore = overView.SupportScore + item.Score;
                else if (salesQuestions.Contains(item.QuestionId)) overView.SalesScore = overView.SalesScore + item.Score;
                else if (scalabilityQuestions.Contains(item.QuestionId)) overView.ScalabilityScore = overView.ScalabilityScore + item.Score;
                else if (appScalabilityQuestions.Contains(item.QuestionId)) overView.AppScalabilityScore = overView.AppScalabilityScore + item.Score;
                else if (extensabilityQuestions.Contains(item.QuestionId)) overView.ExtensibilityScore = overView.ExtensibilityScore + item.Score;
                var currentSuggestion = results.Where(x => x.RowKey == item.QuestionId).FirstOrDefault();
                if (currentSuggestion != null)
                {
                    item.Suggestion = currentSuggestion.Text;
                }
                //item.Suggestion = Get("https://saasjourneyapi.azurewebsites.net/api/GetSuggestionForQuestion?code=JLTch5UXoNlNlaCGdCp1C4X1xtbWcxj2VQvLSIa2hR17TzK/Rseg9A==&question=" + item.QuestionId);
            }
            double percentageCoeficient = 0.8;
            overView.OverallScorePercent = Math.Round((double)overView.OverallScore / 110 * percentageCoeficient * 100);
            overView.BusinessScorePercent = Math.Round((double)overView.BusinessScore / 30 * percentageCoeficient * 100);
            overView.TechnicalScorePercent = Math.Round((double)overView.TechnicalScore / 80 * percentageCoeficient * 100);
            overView.PricingScorePercent = Math.Round((double)overView.PricingScore / 5 * percentageCoeficient * 100);
            overView.SupportScorePercent = Math.Round((double)overView.SupportScore / 11 * percentageCoeficient * 100);
            overView.ScalabilityScorePercent = Math.Round((double)overView.ScalabilityScore / 23 * percentageCoeficient * 100);
            overView.SalesScorePercent = Math.Round((double)overView.SalesScore / 20 * percentageCoeficient * 100);
            overView.AppScalabilityScorePercent = Math.Round((double)overView.AppScalabilityScore / 34 * percentageCoeficient * 100);
            overView.ExtensibilityScorePercent = Math.Round((double)overView.ExtensibilityScore / 7 * percentageCoeficient * 100);
            return req.CreateResponse(HttpStatusCode.OK, overView);
        }
        public static string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                string s = reader.ReadToEnd();
                s = s.Replace("\"", "");
                return s;
            }
        }
        public static List<SuggestionEntity> GetAllSuggestions()
        {
            var connectionString = Environment.GetEnvironmentVariable("SuggestionStorage", EnvironmentVariableTarget.Process);
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudTableClient client = storageAccount.CreateCloudTableClient();
            CloudTable table = client.GetTableReference("suggestions");
            TableQuery<SuggestionEntity> query = new TableQuery<SuggestionEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "suggestions"));
            var results = table.ExecuteQuery(query);
            return results.ToList();
        }
    }
}
