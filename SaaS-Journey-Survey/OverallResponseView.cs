﻿using System.Collections.Generic;

namespace SaaSJourneySurvey
{
    public class OverallResponseView
    {
        public string ResponseId { get; set; }
        public string RespondantEmail { get; set; }
        public string RespondantName { get; set; }
        public string ResponandtLastName { get; set; }
        public string Company { get; set; }
        public string ProductCategory { get; set; }
        public string ProductName { get; set; }
        public int OverallScore { get; set; }
        public double OverallScorePercent { get; set; }
        public int PricingScore { get; set; }
        public double PricingScorePercent { get; set; }
        public int SupportScore { get; set; }
        public double SupportScorePercent { get; set; }
        public int ScalabilityScore { get; set; }
        public double ScalabilityScorePercent { get; set; }
        public int SalesScore { get; set; }
        public double SalesScorePercent { get; set; }
        public int BusinessScore { get; set; }
        public double BusinessScorePercent { get; set; }
        public int TechnicalScore { get; set; }
        public double TechnicalScorePercent { get; set; }
        public int AppScalabilityScore { get; set; }
        public double AppScalabilityScorePercent { get; set; }
        public int ExtensibilityScore { get; set; }
        public double ExtensibilityScorePercent { get; set; }
        public List<MappedResponse> MappedResponses { get; set; }

    }
}
