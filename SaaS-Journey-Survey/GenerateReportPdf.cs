using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using OpenHtmlToPdf;
namespace SaaSJourneySurvey
{
    public static class GenerateReportPdf
    {
        [FunctionName("GenerateReportPdf")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequestMessage req,
            TraceWriter log)
        {
            var html = await req.Content.ReadAsStringAsync();
            var pdf = Pdf.From(html).Content();
            var blobName = Guid.NewGuid().ToString("n") + ".pdf";
            Stream stream = new MemoryStream(pdf);
            var blob = InitiateCloudBlockBlob(blobName).Result;
            await blob.UploadFromStreamAsync(stream);
            var sasBlobToken = GetSasTokenForBlob(blob);
            return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(blob.Uri+sasBlobToken) };
        }
        public static async Task<CloudBlockBlob> InitiateCloudBlockBlob(string name)
        {
            var connectionString = Environment.GetEnvironmentVariable("PdfStorage", EnvironmentVariableTarget.Process);
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient client = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = client.GetContainerReference("saas-journey-reports");
            await container.CreateIfNotExistsAsync();
            CloudBlockBlob blob = container.GetBlockBlobReference(name);
            blob.Properties.ContentType = "application/pdf";
            return blob;
        }
        public static string GetSasTokenForBlob(CloudBlockBlob blob)
        {
            SharedAccessBlobPolicy sasConstraints = new SharedAccessBlobPolicy();
            sasConstraints.SharedAccessStartTime = DateTimeOffset.UtcNow.AddMinutes(-5);
            sasConstraints.SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddHours(24);
            sasConstraints.Permissions = SharedAccessBlobPermissions.Read;
            var sasBlobToken = blob.GetSharedAccessSignature(sasConstraints);
            return sasBlobToken;
        }
    }
}
