using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
//using OpenHtmlToPdf;

namespace SaaSJourneySurvey
{
    public static class ServeHtmlReport
    {
        [FunctionName("ServeHtmlReport")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function,"post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("Started");
           // var textString = File.ReadAllText(@"D:\home\site\wwwroot\HtmlTemplate\app.html");
            var textString = File.ReadAllText(@"C:\Users\AivarsSkridulis\Desktop\app.html");
            dynamic data = await req.Content.ReadAsStringAsync();
            // Get request body
            var e = JsonConvert.DeserializeObject<OverallResponseView>(data as string);
            var listOfSuggestions = "";
            foreach (var q in e.MappedResponses)
            {
                if (q.QuestionId == "q3" && q.Score == 0) listOfSuggestions += "<li>Develop new pricing models</li>";
                if (q.QuestionId == "q4" && q.Score == 0) listOfSuggestions += "<li>Start planning for scale! Rethink your support organization, outsource.</li>";
                if (q.QuestionId == "q5" && q.Score == 0) listOfSuggestions += "<li>Differentiate your offer and reflect that in pricing options.</li>";
                if (q.QuestionId == "q6" && q.Score == 0) listOfSuggestions += "<li>Explore ways to support consumption based pricing.</li>";
                if (q.QuestionId == "q7" && q.Score == 0) listOfSuggestions += "<li>Enable customers to try your product for free!</li>";
                if (q.QuestionId == "q20" && q.Score == 0) listOfSuggestions += "<li>Make sure that all parts of organization is ready for increased customer amounts.</li>";
                if (q.QuestionId == "q21" && q.Score == 0) listOfSuggestions += "<li>Automate onboarding of new customers, minimize dependence on customer actions and person to person. You know what the customer wants!</li>";
                if (q.QuestionId == "q8" && q.Score == 0) listOfSuggestions += "<li>Explore ways to decrease footprint in customer infrastructure by offering hybrid or fully hosted versions of your solution.</li>";
                if (q.QuestionId == "q9" && q.Score == 3) listOfSuggestions += "<li>Evaluate hosting provider ability to support your rapid scale-out and in events.</li>";
                if (q.QuestionId == "q10" && q.Score == 0) listOfSuggestions += "<li>Start moving components from virtual machines to fully managed Platform services (PaaS).</li>";
                if (q.QuestionId == "q11" && q.Score == 0) listOfSuggestions += "<li>Design for storing data closer to user to achieve consistent user experience across the globe.</li>";
                if (q.QuestionId == "q14" && q.Score == 0) listOfSuggestions += "<li>Find partners to assist with customer onboarding and setting up locally installable components, package them for unattended installations.</li>";
                if (q.QuestionId == "q16" && q.Score == 0) listOfSuggestions += "<li>RE-architect application infrastructure to support infrastructure resource sharing. Explore PaaS services supporting this.</li>";
                if (q.QuestionId == "q19" && q.Score == 0) listOfSuggestions += "<li>Choose tools and process to maintain one version of the product for all the customers, no matter the delivery form.</li>";
                if (q.QuestionId == "q23" && q.Score == 0) listOfSuggestions += "<li>Evaluate possibility to transform to a web-based solution, avoid native desktop applications unless absolutely necessary � it will hold you back!</li>";
                if (q.QuestionId == "q25" && q.Score == 0) listOfSuggestions += "<li>Work toward increasing your development cadence. Customers want new features now!</li>";
                textString = textString.Replace("{{"+q.QuestionId.ToUpper()+"}}", q.Suggestion);
            }
            textString = textString.Replace("{{score}}", e.OverallScorePercent.ToString());
            textString = textString.Replace("{{Pricing Score}}", e.PricingScorePercent.ToString());
            textString = textString.Replace("{{Support Score}}", e.SupportScorePercent.ToString());
            textString = textString.Replace("{{Scalability Score}}", e.ScalabilityScorePercent.ToString());
            textString = textString.Replace("{{Sales Score}}", e.SalesScorePercent.ToString());
            textString = textString.Replace("{{AppScalability Score}}", e.AppScalabilityScorePercent.ToString());
            textString = textString.Replace("{{Extensibility Score}}", e.ExtensibilityScorePercent.ToString());
            textString = textString.Replace("{{next_steps}}", listOfSuggestions);
            log.Info("String compiled");
            //start pdf
            //var pdf = Pdf.From(textString).Content();
            //var pdfBlobName = Guid.NewGuid().ToString("n") + ".pdf";
            //Stream stream = new MemoryStream(pdf);
            //var pdfBlob = InitiateCloudBlockBlob(pdfBlobName, "application/pdf").Result;
            //await pdfBlob.UploadFromStreamAsync(stream);
            //var sasPdfBlobToken = GetSasTokenForBlob(pdfBlob);
            //textString = textString.Replace("{{pdf_url}}", pdfBlob.Uri + sasPdfBlobToken);
            //log.Info("PDF Created");
            //end pdf
            //start html
            var blob = InitiateCloudBlockBlob(e.ResponseId+".html", "text/html").Result;
            await blob.UploadTextAsync(textString);
            var sasBlobToken = GetSasTokenForBlob(blob);
            log.Info("HTML Created.");
            //end html
            return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(blob.Uri + sasBlobToken) };
        }
        public static async Task<CloudBlockBlob> InitiateCloudBlockBlob(string name,string type)
        {
            var connectionString = Environment.GetEnvironmentVariable("PdfStorage", EnvironmentVariableTarget.Process);
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient client = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = client.GetContainerReference("survey-results");
            await container.CreateIfNotExistsAsync();
            CloudBlockBlob blob = container.GetBlockBlobReference(name);
            blob.Properties.ContentType = type;
            return blob;
        }
        public static string GetSasTokenForBlob(CloudBlockBlob blob)
        {
            SharedAccessBlobPolicy sasConstraints = new SharedAccessBlobPolicy();
            sasConstraints.SharedAccessStartTime = DateTimeOffset.UtcNow.AddMinutes(-5);
            sasConstraints.SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddHours(24*30);
            sasConstraints.Permissions = SharedAccessBlobPermissions.Read;
            var sasBlobToken = blob.GetSharedAccessSignature(sasConstraints);
            return sasBlobToken;
        }
    }
}
