using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace SaaSJourneySurvey
{
    public static class GetSuggestionForQuestion
    {
        [FunctionName("GetSuggestionForQuestion")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string questionFromRequest = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "question", true) == 0)
                .Value.ToLower();

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();
            var suggestion = GetSuggestion(questionFromRequest); 
            // Set name to query string or body data
            suggestion = suggestion ?? data?.question;

            return questionFromRequest == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, suggestion);
        }
        public static string GetSuggestion(string questionNumber)
        {
            if (String.IsNullOrEmpty(questionNumber))
            {
                return "No question number provided.";
            }
            var connectionString = Environment.GetEnvironmentVariable("SuggestionStorage", EnvironmentVariableTarget.Process);
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudTableClient client = storageAccount.CreateCloudTableClient();
            CloudTable table = client.GetTableReference("suggestions");
            TableOperation retrieveOperation = TableOperation.Retrieve<SuggestionEntity>("suggestions", questionNumber);
            TableResult retrievedResult = table.Execute(retrieveOperation);
            if (retrievedResult.Result != null)
            {
                return ((SuggestionEntity)retrievedResult.Result).Text;
            }
            return "Suggestion not found.";
        }
    }
    public class SuggestionEntity : TableEntity
    {
        public SuggestionEntity(string partitionKey, string questionNumber)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = questionNumber;
        }

        public SuggestionEntity() { }

        public string Text { get; set; }

    }
}
